from abc import ABC
from typing import Any, Dict, List, Optional

import pandas as pd

from config import TableConfig

RowType = Dict[str, Any]


class Table(ABC):
    def get_data(self, _: Optional[List[str]]) -> List[RowType]:
        ...


class FileTable(Table):
    def __init__(self, config: TableConfig):
        self.file = pd.ExcelFile(config.file)
        self.date = self.file.sheet_names[config.sheet]
        self.df = self.file.parse(config.sheet)

    def get_data(self, columns: Optional[List[str]] = None) -> List[RowType]:
        data = self.df[columns] if columns else self.df
        data["date"] = self.date
        return data.to_dict("records")
