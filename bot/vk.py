import random
import re
from typing import Dict, List

import vk_api
from loguru import logger
from pydantic import BaseModel, ValidationError, validator

from config import BotConfig, Config
from table import RowType


class Message(BaseModel):
    username: str
    message: str

    @validator("username")
    def parse_username(cls, v):
        exp = r"(?:.*[\/\@](\w+)|(\w+))"
        r = re.compile(exp)
        results = r.findall(v)
        if not results:
            raise ValidationError(f"invalid vk username: {v}")
        results = results[0]
        if results[0]:
            return results[0]
        elif results[1]:
            return results[1]
        else:
            raise ValidationError(f"invalid vk username: {v}")


def prepare_messages(config: Config, data: List[RowType]) -> List[Message]:
    messages: List[Message] = []
    for row in data:
        text = config.bot.message.format(**row)
        if row[config.table.debt_column] >= config.bot.debt_threshold:
            try:
                m = Message(username=row[config.table.vk_column], message=text)
            except ValidationError as e:
                logger.error(str(e))
            messages.append(m)
    return messages


class Bot:
    def __init__(self, config: BotConfig):
        self.session = vk_api.VkApi(token=config.token)
        self.api = self.session.get_api()

    def __get_random_id(self) -> int:
        return random.randint(0, 2**32)

    def get_users_info(self, usernames: List[str]) -> Dict[str, int]:
        users = self.api.users.get(user_ids=",".join(usernames), fields="screen_name")
        users = {u["screen_name"]: u["id"] for u in users}
        return users

    def send(self, messages: List[Message]) -> List[Message]:
        """
        return: list of unsent messages
        """
        usernames = [m.username for m in messages]
        users = self.get_users_info(usernames)
        errors = []
        for m in messages:
            sent = False
            if m.username in users:
                try:
                    self.api.messages.send(
                        user_id=users[m.username],
                        message=m.message,
                        random_id=self.__get_random_id(),
                    )
                    sent = True
                except Exception as e:
                    logger.error(f"sending message to {m.username}: {str(e)}")
            if not sent:
                logger.error(f"User {m.username} not found.")
                errors.append(m)
        return errors
