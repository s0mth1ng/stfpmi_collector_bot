import yaml
from pydantic import BaseModel


class BotConfig(BaseModel):
    token: str
    message: str
    debt_threshold: float = 0


class TableConfig(BaseModel):
    file: str
    sheet: int
    vk_column: str
    debt_column: str


class Config(BaseModel):
    bot: BotConfig
    table: TableConfig


def get_config(file) -> Config:
    return Config(**yaml.safe_load(file))
