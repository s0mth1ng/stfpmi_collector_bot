import argparse

from loguru import logger

from config import Config, get_config
from table import FileTable
from vk import Bot, prepare_messages


def init_parser():
    parser = argparse.ArgumentParser(description="Send some debts.")
    parser.add_argument(
        "--config",
        metavar="config",
        type=argparse.FileType("r"),
        nargs="?",
        help="filepath to config in yaml format",
        default="config.yaml",
    )
    return parser.parse_args()


def run_job(config: Config):
    table = FileTable(config.table)
    data = table.get_data()
    messages = prepare_messages(config, data)
    bot = Bot(config.bot)
    not_sent = bot.send(messages)
    if not_sent:
        logger.warning(f"Some messages were not delivered: {not_sent}")
    else:
        logger.info("All messages have been sent.")


def main():
    args = init_parser()
    config = get_config(args.config)
    run_job(config)


if __name__ == "__main__":
    main()
